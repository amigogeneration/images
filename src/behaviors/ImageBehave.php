<?php

namespace AmigoGeneration\images\behaviors;

use AmigoGeneration\images\models\Image;

use yii;
use yii\base\Behavior;
use yii\base\Exception;
use yii\db\ActiveRecord;
use AmigoGeneration\images\models;
use yii\helpers\BaseArrayHelper;
use yii\helpers\BaseFileHelper;
use \AmigoGeneration\images\ModuleTrait;

/**
 *
 * @property ActiveRecord|null|array $image
 * @property mixed $mainImage
 * @property array|ActiveRecord[] $images
 * @property string $alias
 * @property null|string $aliasString
 */
class ImageBehave extends Behavior
{
    use ModuleTrait;
    public $createAliasMethod = false;

    /**
     * @var ActiveRecord|null Model class, which will be used for storing image data in db, if not set default class(models/Image) will be used
     */

    /**
     *
     * Method copies image file to module store and creates db record.
     *
     * @param $absolutePath
     * @param bool $isMain
     * @param string $name
     * @return bool|Image
     * @throws yii\base\Exception
     * @throws Exception
     * @throws \Exception
     */
    public function attachImage($absolutePath, $isMain = false, $name = '')
    {
        if ((false === strpos($absolutePath, 'http')) && !file_exists($absolutePath)) {
            throw new Exception('File not exist! :' . $absolutePath);
        }

        if (!$this->owner->primaryKey) {
            throw new Exception('Owner must have primaryKey when you attach image!');
        }

        $pictureFileName =
            substr(md5(microtime(true) . $absolutePath), 4, 6)
            . '.' .
            pathinfo($absolutePath, PATHINFO_EXTENSION);
        $pictureSubDir = $this->getModule()->getModelSubDir($this->owner);
        $storePath = $this->getModule()->getStorePath();

        $newAbsolutePath = $storePath .
            DIRECTORY_SEPARATOR . $pictureSubDir .
            DIRECTORY_SEPARATOR . $pictureFileName;

        BaseFileHelper::createDirectory($storePath . DIRECTORY_SEPARATOR . $pictureSubDir);

        copy($absolutePath, $newAbsolutePath);

        if (!file_exists($newAbsolutePath)) {
            throw new Exception('Cant copy file! ' . $absolutePath . ' to ' . $newAbsolutePath);
        }

        if ($this->getModule()->className === null) {
            $image = new models\Image;
        } else {
            $class = $this->getModule()->className;
            $image = new $class();
        }
        $image->itemId = $this->owner->primaryKey;
        $image->filePath = $pictureSubDir . '/' . $pictureFileName;
        $image->modelName = $this->getModule()->getShortClass($this->owner);
        $image->name = $name;

        $image->urlAlias = $this->getAlias();

        if (!$image->save()) {
            return false;
        }

        if (count($image->getErrors()) > 0) {

            $ar = array_shift($image->getErrors());

            unlink($newAbsolutePath);
            throw new Exception(array_shift($ar));
        }
        $img = $this->owner->getImage();

        //If main image not exists
        if ((is_object($img) && get_class($img) === 'AmigoGeneration\images\models\PlaceHolder') || !$img || $isMain) {
            $this->setMainImage($image);
        }
        return $image;
    }

    /**
     * Sets main image of model
     * @param $img
     * @throws Exception
     */
    public function setMainImage($img): void
    {
        if ($this->owner->primaryKey !== $img->itemId) {
            throw new Exception('Image must belong to this model');
        }
        $counter = 1;
        /* @var $img Image */
        $img->setMain(true);
        $img->urlAlias = $this->getAliasString() . '-' . $counter;
        $img->save();

        $images = $this->owner->getImages();
        foreach ($images as $allImg) {

            if ($allImg->id === $img->id) {
                continue;
            }

            $counter++;

            $allImg->setMain(false);
            $allImg->urlAlias = $this->getAliasString() . '-' . $counter;
            $allImg->save();
        }

        $this->owner->clearImagesCache();
    }

    /**
     * Clear all images cache (and resized copies)
     * @return bool
     * @throws yii\base\ErrorException
     * @throws yii\base\Exception
     */
    public function clearImagesCache(): bool
    {
        $cachePath = $this->getModule()->getCachePath();
        $subDir = $this->getModule()->getModelSubDir($this->owner);

        $dirToRemove = $cachePath . '/' . $subDir;

        if (preg_match('/' . preg_quote($cachePath, '/') . '/', $dirToRemove)) {
            BaseFileHelper::removeDirectory($dirToRemove);
            return true;
        }

        return false;
    }

    /**
     * Returns model images
     * First image alwats must be main image
     * @return array|ActiveRecord[]
     * @throws yii\base\Exception
     */
    public function getImages(): array
    {
        $finder = $this->getImagesFinder();

        if ($this->getModule()->className === null) {
            $imageQuery = Image::find();
        } else {
            $class = $this->getModule()->className;
            $imageQuery = $class::find();
        }
        $imageQuery->where($finder);
        //$imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);
        $imageQuery->orderBy(['sort' => SORT_ASC]);

        $imageRecords = $imageQuery->all();
        if (!$imageRecords) {
            return [$this->getModule()->getPlaceHolder()];
        }
        return $imageRecords;
    }

    /**
     * returns main model image
     * @return array|null|ActiveRecord
     * @throws yii\base\Exception
     */
    public function getImage()
    {
        if ($this->getModule()->className === null) {
            $imageQuery = Image::find();
        } else {
            /** @var ActiveRecord $class */
            $class = $this->getModule()->className;
            $imageQuery = $class::find();
        }
        $finder = $this->getImagesFinder(['isMain' => 1]);
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $img = $imageQuery->one();
        if (!$img) {
            return $this->getModule()->getPlaceHolder();
        }

        return $img;
    }

    /**
     * @param $sort
     * @param bool $returnPlaceHolder
     * @return array|models\PlaceHolder|ActiveRecord|null
     * @throws yii\base\Exception
     */
    public function getImageBySort($sort, $returnPlaceHolder = true)
    {
        if ($this->getModule()->className === null) {
            $imageQuery = Image::find();
        } else {
            /** @var ActiveRecord $class */
            $class = $this->getModule()->className;
            $imageQuery = $class::find();
        }

        $imageQuery->andWhere([
            'itemId' => $this->owner->primaryKey,
            'modelName' => $this->getModule()->getShortClass($this->owner),
            'sort' => $sort
        ]);

        $img = $imageQuery->one();
        if (!$img && $returnPlaceHolder) {
            return $this->getModule()->getPlaceHolder();
        }

        return $img;
    }

    /**
     * returns model image by name
     * @param $name
     * @return array|null|ActiveRecord
     * @throws yii\base\Exception
     */
    public function getImageByName($name)
    {
        if ($this->getModule()->className === null) {
            $imageQuery = Image::find();
        } else {
            /** @var ActiveRecord $class */
            $class = $this->getModule()->className;
            $imageQuery = $class::find();
        }
        $finder = $this->getImagesFinder(['name' => $name]);
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $img = $imageQuery->one();
        if (!$img) {
            return $this->getModule()->getPlaceHolder();
        }

        return $img;
    }

    /**
     * @return bool
     * @throws yii\base\ErrorException
     * @throws yii\base\Exception
     */
    public function removeImages(): ?bool
    {
        $images = $this->owner->getImages();
        if (count($images) < 1) {
            return true;
        }

        foreach ($images as $image) {
            $this->owner->removeImage($image);
        }
        $storePath = $this->getModule()->getStorePath($this->owner);
        $pictureSubDir = $this->getModule()->getModelSubDir($this->owner);
        $dirToRemove = $storePath . DIRECTORY_SEPARATOR . $pictureSubDir;
        BaseFileHelper::removeDirectory($dirToRemove);
        return true;
    }

    /**
     * removes concrete model's image
     * @param Image $img
     * @return bool
     * @throws \Throwable
     * @throws yii\base\ErrorException
     * @throws yii\base\Exception
     * @throws yii\db\StaleObjectException
     */
    public function removeImage(Image $img): bool
    {
        if ($img instanceof models\PlaceHolder) {
            return false;
        }
        $img->clearCache();

        $storePath = $this->getModule()->getStorePath();

        $fileToRemove = $storePath . DIRECTORY_SEPARATOR . $img->filePath;
        if (false !== strpos($fileToRemove, '.') && is_file($fileToRemove)) {
            unlink($fileToRemove);
        }
        $img->delete();
        return true;
    }

    /**
     * @param bool $additionWhere
     * @return array
     * @throws yii\base\Exception
     */
    private function getImagesFinder($additionWhere = false): array
    {
        $base = [
            'itemId' => $this->owner->primaryKey,
            'modelName' => $this->getModule()->getShortClass($this->owner)
        ];

        if ($additionWhere) {
            $base = BaseArrayHelper::merge($base, $additionWhere);
        }
        return $base;
    }

    /** Make string part of image's url
     * @return string
     * @throws Exception
     */
    private function getAliasString(): ?string
    {
        if ($this->createAliasMethod) {
            $string = $this->owner->{$this->createAliasMethod}();
            if (!is_string($string)) {
                throw new Exception("Image's url must be string!");
            }
            return $string;
        }
        return substr(md5(microtime()), 0, 10);
    }

    /**
     *
     * Обновить алиасы для картинок
     * Зачистить кэш
     * @throws \Exception
     */
    private function getAlias(): string
    {
        $images = $this->owner->getImages();
        if ($images && !is_array($images)){
            $images = $images->all();
        }
        $imagesCount = count($images) + 1;
        return $this->getAliasString() . '-' . $imagesCount;
    }
}
