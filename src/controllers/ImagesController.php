<?php

namespace AmigoGeneration\images\controllers;

use Exception;
use yii\web\Controller;
use yii;
use \AmigoGeneration\images\ModuleTrait;
use yii\web\HttpException;
use yii\filters\PageCache;

/**
 * Class ImagesController
 * @package AmigoGeneration\images\controllers
 */
class ImagesController extends Controller
{
    use ModuleTrait;

    public function actionIndex(): void
    {
        echo "Hello, man. It's ok, dont worry.";
    }

    public function actionTestTest(): void
    {
        echo "Hello, man. It's ok, dont worry.";
    }

    /**
     * @return array
     */
    public function behaviors():array
    {
        return [
            [
                'class' => PageCache::class,
                'duration' => 3600,
                'only' => ['image-by-item-and-alias'],
                'variations' => [
                    Yii::$app->request->get('item'),
                    Yii::$app->request->get('dirtyAlias'),
                    Yii::$app->request->get('logo'),
                ],
            ]
        ];
    }

    /**
     *
     * All we need is love. No.
     * We need item (by id or another property) and alias (or images number)
     * @param string $item
     * @param $dirtyAlias
     * @param bool $logo
     * @return yii\console\Response|yii\web\Response
     * @throws HttpException
     * @throws yii\base\Exception
     * @throws Exception
     */
    public function actionImageByItemAndAlias($item, $dirtyAlias, $logo = false)
    {

        $dotParts = explode('.', $dirtyAlias);
        if (!isset($dotParts[1])) {
            throw new HttpException(404, 'Image must have extension');
        }
        $dirtyAlias = $dotParts[0];

        $size = explode('_', $dirtyAlias)[1] ?? false;
        $alias = explode('_', $dirtyAlias)[0] ?? false;
        $image = $this->getModule()->getImage($item, $alias);

        if ($image->getExtension() !== $dotParts[1]) {
            throw new HttpException(404, 'Image not found (extension)');
        }
        if ($image) {
            \Yii::$app->response->format = yii\web\Response::FORMAT_RAW;
            \Yii::$app->response->headers->add('content-type','image/png');
            \Yii::$app->response->headers->add('Cache-Control','max-age=604800');
            \Yii::$app->response->data = $image->getContent($size, $logo);
            return \Yii::$app->response;
        }
        throw new HttpException(404, 'There is no images');
    }
}