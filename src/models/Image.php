<?php

namespace AmigoGeneration\images\models;

use abeautifulsite\SimpleImage;
use common\models\Offer;
use Imagick;
use ImagickException;
use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\helpers\BaseFileHelper;
use \AmigoGeneration\images\ModuleTrait;

/**
 * Class Image
 * @package AmigoGeneration\images\models
 *
 * @property string $id
 * @property string $filePath путь к файлу
 * @property int $itemId ID владельца
 * @property int $isMain Это главное изображение
 * @property string $modelName Модель владельца
 * @property string $urlAlias
 * @property string $name Наименование
 * @property bool $main
 * @property string $subDur
 * @property string $pathToOrigin
 * @property bool|array $sizes
 * @property mixed $extension
 * @property int $sort Сортировка
 */
class Image extends ActiveRecord
{
    use ModuleTrait;

    private $helper = false;

    public $imageLogo = false;

    /**
     * @return bool
     * @throws Exception
     * @throws ErrorException
     */
    public function clearCache()
    {
        $subDir = $this->getSubDur();

        $dirToRemove = $this->getModule()->getCachePath() . DIRECTORY_SEPARATOR . $subDir;

        if (preg_match('/' . preg_quote($this->modelName, '/') . '/', $dirToRemove)) {
            BaseFileHelper::removeDirectory($dirToRemove);
        }
        return true;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getExtension()
    {
        return pathinfo($this->getPathToOrigin(), PATHINFO_EXTENSION);
    }

    /**
     * @param bool $size
     * @param bool $logo
     * @return string
     * @throws Exception
     */
    public function getUrl($size = false, $logo = false)
    {
        $urlSize = $size ? '_' . $size : '';
        $url = Url::toRoute([
            '/' . $this->getModule()->id . '/images/image-by-item-and-alias',
            'item' => $this->modelName . $this->itemId,
            'dirtyAlias' => $this->urlAlias . $urlSize . '.' . $this->getExtension(),
            'logo' => $logo
        ]);

        return $url;
    }

    /**
     * @param bool $size Размер изображения
     * @param bool $logo Показывать логотип если установлен
     * @return string
     * @throws Exception
     * @throws \Exception
     */
    public function getPath($size = false, $logo = false)
    {
        $urlSize = $size ? '_' . $size : '';
        $urlSize .= $logo ? '_logo' : '';

        $base = $this->getModule()->getCachePath();
        $sub = $this->getSubDur();

        $origin = $this->getPathToOrigin();

        $filePath = $base . $sub . DIRECTORY_SEPARATOR . $this->urlAlias . $urlSize . '.' . pathinfo($origin, PATHINFO_EXTENSION);


        if (!file_exists($filePath)) {
            $this->createVersion($origin, $size, $logo);

            if (!file_exists($filePath)) {
                throw new \Exception('Problem with image creating.');
            }
        }

        $filePath = Yii::getAlias('@web/images/cache' . DIRECTORY_SEPARATOR .
            $sub . DIRECTORY_SEPARATOR . $this->urlAlias . $urlSize . '.' . pathinfo($origin, PATHINFO_EXTENSION));

        return $filePath;
    }

    public function getRealPath($size = false, $logo = false)
    {
        $urlSize = $size ? '_' . $size : '';
        $urlSize .= $logo ? '_logo' : '';

        $base = $this->getModule()->getCachePath();
        $sub = $this->getSubDur();

        $origin = $this->getPathToOrigin();

        $filePath = $base . $sub . DIRECTORY_SEPARATOR . $this->urlAlias . $urlSize . '.' . pathinfo($origin, PATHINFO_EXTENSION);


        if (!file_exists($filePath)) {
            $this->createVersion($origin, $size, $logo);

            if (!file_exists($filePath)) {
                throw new \Exception('Problem with image creating.');
            }
        }

        return $filePath;
    }

    /**
     * @param bool $size
     * @param bool $logo
     * @return false|string
     * @throws Exception
     */
    public function getContent($size = false, $logo = false)
    {
        return file_get_contents($this->getPath($size, $logo));
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getPathToOrigin()
    {
        $base = $this->getModule()->getStorePath();
        return $base . DIRECTORY_SEPARATOR . $this->filePath;
    }

    /**
     * @return array|bool
     * @throws Exception
     * @throws ImagickException
     * @throws \Exception
     */
    public function getSizes()
    {
        $sizes = false;
        if (file_exists($this->getPathToOrigin())) {
            if ($this->getModule()->graphicsLibrary === 'Imagick') {
                $image = new Imagick($this->getPathToOrigin());
                $sizes = $image->getImageGeometry();
            } else {
                $image = new SimpleImage($this->getPathToOrigin());
                $sizes['width'] = $image->get_width();
                $sizes['height'] = $image->get_height();
            }
        }


        return $sizes;
    }

    /**
     * @param $sizeString
     * @return array
     * @throws Exception
     * @throws ImagickException
     * @throws \Exception
     */
    public function getSizesWhen($sizeString)
    {

        $size = $this->getModule()->parseSize($sizeString);
        if (!$size) {
            throw new \Exception('Bad size..');
        }

        $sizes = $this->getSizes();

        $imageWidth = $sizes['width'];
        $imageHeight = $sizes['height'];
        $newSizes = [];
        if (!$size['width']) {
            $newWidth = $imageWidth * ($size['height'] / $imageHeight);
            $newSizes['width'] = (int)$newWidth;
            $newSizes['height'] = $size['height'];
        } elseif (!$size['height']) {
            $newHeight = (int)($imageHeight * ($size['width'] / $imageWidth));
            $newSizes['width'] = $size['width'];
            $newSizes['height'] = $newHeight;
        }
        return $newSizes;
    }

    /**
     * @param $imagePath
     * @param bool $sizeString
     * @param bool $logo
     * @return SimpleImage|Imagick
     * @throws Exception
     * @throws ImagickException
     * @throws \Exception
     */
    public function createVersion($imagePath, $sizeString = false, $logo = false)
    {
        if ($this->urlAlias === '') {
            throw new \Exception('Image without urlAlias!');
        }

        $cachePath = $this->getModule()->getCachePath();
        $subDirPath = $this->getSubDur();
        $fileExtension = pathinfo($this->filePath, PATHINFO_EXTENSION);

        if ($sizeString) {
            $sizePart = '_' . $sizeString;
        } else {
            $sizePart = '';
        }

        $sizePart .= $logo ? '_logo' : '';

        $pathToSave = $cachePath . $subDirPath . '/' . $this->urlAlias . $sizePart . '.' . $fileExtension;
        BaseFileHelper::createDirectory(dirname($pathToSave), 0777);

        if ($sizeString) {
            $size = $this->getModule()->parseSize($sizeString);
        } else {
            $size = false;
        }

        if ($this->getModule()->graphicsLibrary === 'Imagick') {
            $image = new Imagick($imagePath);
            $image->setImageCompressionQuality(Yii::$app->params['image_compression_quality']);

            if ($size) {
                if ($size['height'] && $size['width']) {
                    $image->cropThumbnailImage($size['width'], $size['height']);
                } elseif ($size['height']) {
                    $image->thumbnailImage(0, $size['height']);
                } elseif ($size['width']) {
                    $image->thumbnailImage($size['width'], 0);
                } else {
                    throw new \Exception('Something wrong with this->module->parseSize($sizeString)');
                }
            }
            $image->writeImage($pathToSave);
        } else {
            $image = new \abeautifulsite\SimpleImage($imagePath);
            if ($size) {
                if ($size['height'] && $size['width']) {

                    $image->thumbnail($size['width'], $size['height']);
                } elseif ($size['height']) {
                    $image->fit_to_height($size['height']);
                } elseif ($size['width']) {
                    $image->fit_to_width($size['width']);
                } else {
                    throw new \Exception('Something wrong with this->module->parseSize($sizeString)');
                }
            }
            //urlAlias placeHolder
            //todo костыль для логотипов, надо красивое решение

            if ($logo && $this->modelName === 'Offer') {

                $imageLogo = Offer::findOne(['id' => $this->itemId])->company->getImage();
                if ($imageLogo->urlAlias !== 'placeHolder') {

                    $height = (int)$size['height'];
                    $width = (int)$size['width'];
                    $logoHeight = (int)($width / 4);
                    $logoOffset = (int)($width / 18.3);
                    $logoBgOffset = (int)($width / 55);
                    $imageLogoPath = $imageLogo->getRealPath('x' . $logoHeight);
                    if (!file_exists($imageLogoPath)) {
                        throw new Exception('Logo not detected!');
                    }
                    $logoBgPath = Yii::getAlias($this->getModule()->logoBgPath);
                    if (file_exists($logoBgPath)) {
                        $logoBg = new \abeautifulsite\SimpleImage($logoBgPath);
                        $logoBg->thumbnail($logoHeight + $logoBgOffset * 2, $logoHeight);
                        $logoBg->overlay($imageLogoPath, 'bottom right', 1, -$logoBgOffset, 0);
                        $imageLogoPath = $logoBg;
                    }

                    $image->overlay($imageLogoPath, 'bottom right', 1, -$logoOffset * 2, -$logoOffset);
                }
            }

            //WaterMark
            if ($this->getModule()->waterMark) {

                if (!file_exists(Yii::getAlias($this->getModule()->waterMark))) {
                    throw new Exception('WaterMark not detected!');
                }

                $wmMaxWidth = (int)($image->get_width() * 0.4);
                $wmMaxHeight = (int)($image->get_height() * 0.4);

                $waterMarkPath = Yii::getAlias($this->getModule()->waterMark);
                $waterMark = new SimpleImage($waterMarkPath);

                if (
                    $waterMark->get_height() > $wmMaxHeight
                    or
                    $waterMark->get_width() > $wmMaxWidth
                ) {

                    $waterMarkPath = $this->getModule()->getCachePath() . DIRECTORY_SEPARATOR .
                        pathinfo($this->getModule()->waterMark)['filename'] .
                        $wmMaxWidth . 'x' . $wmMaxHeight . '.' .
                        pathinfo($this->getModule()->waterMark)['extension'];

                    //throw new Exception($waterMarkPath);
                    if (!file_exists($waterMarkPath)) {
                        $waterMark->fit_to_width($wmMaxWidth);
                        $waterMark->save($waterMarkPath, 100);
                        if (!file_exists($waterMarkPath)) {
                            throw new Exception('Cant save watermark to ' . $waterMarkPath . '!!!');
                        }
                    }
                }
                $image->overlay($waterMarkPath, 'bottom right', 1, -10, -20);
            }
            $image->save($pathToSave, 100);
        }
        return $image;
    }

    /**
     * @param bool $isMain
     */
    public function setMain($isMain = true) : void
    {
        if ($isMain) {
            $this->isMain = 1;
        } else {
            $this->isMain = 0;
        }
    }

    /**
     * @return string
     */
    protected function getSubDur(): string
    {
        return Inflector::pluralize($this->modelName) . '/' . $this->modelName . $this->itemId;
    }


    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{%image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['filePath', 'itemId', 'modelName', 'urlAlias'], 'required'],
            [['itemId', 'isMain'], 'integer'],
            [['name'], 'string', 'max' => 80],
            [['filePath', 'urlAlias'], 'string', 'max' => 400],
            [['modelName'], 'string', 'max' => 150],
            [['sort'], 'default', 'value' => static function ($model) {
                return Image::find()
                    ->andWhere(['modelName' => $model->modelName, 'itemId' => $model->itemId])
                    ->count();
            }]
        ];
    }
}
