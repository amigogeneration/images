<?php

namespace AmigoGeneration\images\models;

/**
 * TODO: check path to save and all image method for placeholder
 */

use yii;
use yii\base\Exception;

/**
 * Class PlaceHolder
 * @package AmigoGeneration\images\models
 */
class PlaceHolder extends Image
{
    public $filePath = 'placeHolder.png';
    public $urlAlias = 'placeHolder';

    /**
     * PlaceHolder constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->filePath = basename(Yii::getAlias($this->getModule()->placeHolderPath));
    }

    /**
     * @return bool|string
     * @throws Exception
     * @throws \Exception
     */
    public function getPathToOrigin()
    {
        $url = Yii::getAlias($this->getModule()->placeHolderPath);
        if (!$url) {
            throw new \Exception('PlaceHolder image must have path setting!!!');
        }
        return $url;
    }

    /**
     * @return string
     */
    protected function getSubDur(): string
    {
        return 'placeHolder';
    }

    /**
     * @param bool $isMain
     * @throws Exception
     */
    public function setMain($isMain = true) : void
    {
        throw new Exception('You must not set placeHolder as main image!!!');
    }

}

