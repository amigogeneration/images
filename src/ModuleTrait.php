<?php
namespace AmigoGeneration\images;

use Yii;
use yii\base\Exception;

/**
 * Trait ModuleTrait
 * @package AmigoGeneration\images
 */
trait ModuleTrait
{
    /**
     * @var null|ImageModule
     */
    private $_module;

    /**
     * @return ImageModule
     * @throws Exception
     */
    protected function getModule(): ImageModule
    {
        if ($this->_module === null) {
            $this->_module = Yii::$app->getModule('images');
        }

        if(!$this->_module){
            throw new Exception("\n\n\n\n\nImages module not found, may be you didn't add it to your config?\n\n\n\n");
        }

        return $this->_module;
    }
}