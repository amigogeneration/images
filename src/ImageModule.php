<?php

namespace AmigoGeneration\images;

use AmigoGeneration\images\models\PlaceHolder;
use yii;
use AmigoGeneration\images\models\Image;
use yii\base\Exception;
use yii\base\Module as BaseModule;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;

/**
 * Class Module
 * @package AmigoGeneration\images
 *
 * @property bool|string $storePath
 * @property bool|string $cachePath
 * @property null|PlaceHolder $placeHolder
 */
class ImageModule extends BaseModule
{
    public $imagesStorePath = '@app/web/store';

    public $imagesCachePath = '@app/web/imgCache';

    public $graphicsLibrary = 'GD';

    public $controllerNamespace = 'AmigoGeneration\images\controllers';

    public $placeHolderPath;

    public $waterMark = false;

    public $className;

    public $access = [];

    public $logoBgPath = '@common/modules/images/assets/img/logo-bg.png';

    /**
     * @param $item
     * @param $dirtyAlias
     * @return array|PlaceHolder|yii\db\ActiveRecord|null
     * @throws \Exception
     */
    public function getImage($item, $dirtyAlias)
    {
        //Get params
        $params = $this->parseImageAlias($dirtyAlias);

        $alias = $params['alias'];

        $itemId = preg_replace('/[^\D]+/', '', $item);
        $modelName = preg_replace('/[\D]+/', '', $item);

        if (empty($this->className)) {
            $imageQuery = Image::find();
        } else {
            /** @var ActiveRecord $class */
            $class = $this->className;
            $imageQuery = $class::find();
        }
        $image = $imageQuery
            ->andWhere([
                'modelName' => $modelName,
                'itemId' => $itemId,
                'urlAlias' => $alias
            ])
            ->one();
        if (!$image) {
            return $this->getPlaceHolder();
        }

        return $image;
    }

    /**
     * @return bool|string
     */
    public function getStorePath()
    {
        return Yii::getAlias($this->imagesStorePath);
    }

    /**
     * @return bool|string
     */
    public function getCachePath()
    {
        return Yii::getAlias($this->imagesCachePath);
    }

    /**
     * @param $model
     * @return string
     */
    public function getModelSubDir($model): string
    {
        $modelName = $this->getShortClass($model);
        return Inflector::pluralize($modelName) . '/' . $modelName . $model->id;
    }

    /**
     * @param $obj
     * @return string
     */
    public function getShortClass($obj): string
    {
        $className = get_class($obj);
        if (preg_match('@\\\\([\w]+)$@', $className, $matches)) {
            $className = $matches[1];
        }
        return $className;
    }

    /**
     *
     * Parses size string
     * For instance: 400x400, 400x, x400
     *
     * @param $notParsedSize
     * @return array|null
     * @throws Exception
     */
    public function parseSize($notParsedSize): ?array
    {
        $sizeParts = explode('x', $notParsedSize);
        $part1 = (isset($sizeParts[0]) and $sizeParts[0] !== '');
        $part2 = (isset($sizeParts[1]) and $sizeParts[1] !== '');
        if ($part1 && $part2) {
            if ((int)$sizeParts[0] > 0 && (int)$sizeParts[1] > 0) {
                $size = [
                    'width' => (int)$sizeParts[0],
                    'height' => (int)$sizeParts[1]
                ];
            } else {
                $size = null;
            }
        } elseif ($part1 && !$part2) {
            $size = [
                'width' => (int)$sizeParts[0],
                'height' => null
            ];
        } elseif (!$part1 && $part2) {
            $size = [
                'width' => null,
                'height' => (int)$sizeParts[1]
            ];
        } else {
            throw new Exception('Something bad with size, sorry!');
        }

        return $size;
    }

    /**
     * @param $parameterized
     * @return array
     * @throws \Exception
     */
    public function parseImageAlias($parameterized): array
    {
        $params = explode('_', $parameterized);

        if (count($params) === 1) {
            $alias = $params[0];
            $size = null;
        } elseif (count($params) === 2) {
            $alias = $params[0];
            $size = $this->parseSize($params[1]);
            if (!$size) {
                $alias = null;
            }
        } else {
            $alias = null;
            $size = null;
        }
        return ['alias' => $alias, 'size' => $size];
    }

    /**
     * @throws Exception
     */
    public function init(): void
    {
        parent::init();
        if (!$this->imagesStorePath || !$this->imagesCachePath || $this->imagesStorePath === '@app' || $this->imagesCachePath === '@app') {
            throw new Exception('Setup imagesStorePath and imagesCachePath images module properties!!!');
        }
    }

    /**
     * @return PlaceHolder|null
     * @throws Exception
     */
    public function getPlaceHolder(): ?PlaceHolder
    {
        if ($this->placeHolderPath) {
            return new PlaceHolder();
        }
        return null;
    }
}
