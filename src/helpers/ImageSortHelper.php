<?php
namespace AmigoGeneration\images\helpers;

use AmigoGeneration\images\models\Image;
use yii\web\NotFoundHttpException;

class ImageSortHelper
{
    /**
     * @param array $sortArr
     * @return bool
     * @throws NotFoundHttpException
     */
    public static function sortImage($sortArr): bool
    {
        if (!is_array($sortArr) || count($sortArr) === 0) {
            //todo исключения
            throw new NotFoundHttpException('Нет массива изображений.');
        }
        //todo одним запросом?
        //todo таранзакция нужна?
        foreach ($sortArr as $sort => $id) {
            Image::updateAll(['sort' => $sort, 'isMain' => (int)$sort === 0 ? 1 : 0], ['id' => $id]);
        }
        return true;
    }
}