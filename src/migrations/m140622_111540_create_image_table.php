<?php

use yii\db\Migration;

class m140622_111540_create_image_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%image}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(80),
            'filePath' => $this->string(400)->notNull(),
            'itemId' => $this->integer(),
            'isMain' => $this->boolean(),
            'modelName' => $this->string(150)->notNull(),
            'urlAlias' => $this->string(400)->notNull(),
            'sort' => $this->integer()->defaultValue(0)
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%image}}');
    }
}
